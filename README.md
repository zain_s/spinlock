# Spinlock
```c++

#include "spin_lock.hpp"

int main() {
    using spin_lock = spin_ttas::spin_lock;
    spin_lock l;
    l.lock();
    // critical operation
    l.unlock();
    if (l.trylock()){
        // critical operation
        l.unlock();
    } // if
    return 0;
}

```

https://gitlab.com/zain_s/spinlock
