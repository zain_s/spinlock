#include<iostream>
#include <thread>
#include <vector>
#include <iomanip>
#include <chrono>
#include <mutex>

#include "spin_lock.hpp"

/* 
 * The spin lock test is based on:
 * https://www.talkinghightech.com/en/implementing-a-spinlock-in-c/
 */

const int count_to = 100000;
int value = 0;

template<typename t, typename lock_type>
int task(t count_to, lock_type& l) {

    std::cout << "Started  " << count_to << std::endl;
    for (int i = 0; i < count_to; ++i)
    {

        {
            const std::lock_guard<lock_type> lock(l);
            value++;
            std::this_thread::sleep_for(std::chrono::microseconds(200));
        }

    }
    return 0;
}

void check(const int num_workers) {
    if (value == count_to * num_workers) {
        std::cout << "Lock passed" << std::endl;
    }
    else {
        std::cout << "Lock failed" << std::endl;
    }
} // check

int main() {
    const int num_workers = 8;
    // ==================================================Spinlock==========================================================
    std::vector<std::thread> threads;
    std::cout << "spinLock start...." << std::endl;
    value = 0;

    using spin_type = spin_ttas::spin_lock;
    spin_type l;

    auto t0 = std::chrono::steady_clock::now();
    for (int i = 0; i < num_workers; ++i)
        threads.push_back(std::move(std::thread(task<int, spin_type>, count_to, std::ref(l))));

    for (auto& it : threads) it.join();

    auto t1 = std::chrono::steady_clock::now();
    std::chrono::duration<double> T = t1 - t0;

    std::cout << "job done, "
              << "count_: " << value << std::endl;

    auto d0 = T.count();
    std::cout << "time to test: " << d0 << "s" << std::endl;
    check(num_workers);

    // ==================================================MUTEX==========================================================
    std::cout << "\nmutex start...." << std::endl;
    threads.clear();
    value = 0;

    using mtx_type = std::mutex;
    mtx_type m;

    t0 = std::chrono::steady_clock::now();
    for (int i = 0; i < num_workers; ++i)
        threads.push_back(std::move(std::thread(task<int, mtx_type>, count_to, std::ref(m))));

    for (auto& it : threads) it.join();
    t1 = std::chrono::steady_clock::now();
    T = t1 - t0;
    std::cout << "job done, "
              << "count_: " << value << std::endl;

    auto d1 = T.count();
    std::cout << "time to test: " << d1 << "s" << std::endl;
    check(num_workers);

    std::cout << "\nimprovement of spinlock is: " << ((d1 - d0) / d1 * 100) <<"%\n";

    return 0;
}