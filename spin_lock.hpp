/***
 *  $Id$
 **
 *  File: spin_lock.hpp
 *  Created: June 03, 2022
 *
 *  Author: Zainul Abideen Sayed <zsayed@buffalo.edu>
 * 
 *  Copyright (c) 2020-2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 * 
 */

#ifndef SPIN_LOCK_HPP
#define SPIN_LOCK_HPP

#include <thread>
#include <atomic>
#include <iostream>

#if defined(__i386__) || defined(__x86_64__)
    #include <xmmintrin.h>
# endif

/*
 * Test-and-Test-And-Swap(TTAS) spinlock
 *
 * usage: spin_ttas::spin_lock
 *
 * The spin_lock is based on:
 * E.Rigtorp: Correctly implementing a spinlock in C++
 * https://rigtorp.se/spinlock/
 */

namespace spin_ttas {

    inline void spin_pause() {
        #if defined(__i386__) || defined(__x86_64__)
                _mm_pause();
         #elif defined(__aarch64__) || defined(__arm__)
            __asm__ __volatile__("isb\n");
        #else
            std::this_thread::yield();
        # endif
    } // spin_pause


    struct spin_lock {
      std::atomic<bool> lock_ = {0};

      void lock() noexcept {
        for (;;) {
          // Optimistically assume the lock is free on the first try
          if (!lock_.exchange(true, std::memory_order_acquire)) {
            return;
          }
          // Wait for lock to be released without generating cache misses
          while (lock_.load(std::memory_order_relaxed)) spin_pause();
        }
      } // lock

      bool try_lock() noexcept {
        // First do a relaxed load to check if lock is free in order to prevent
        // unnecessary cache misses if someone does while(!try_lock())

        return !lock_.load(std::memory_order_relaxed) &&
               !lock_.exchange(true, std::memory_order_acquire);
      } // try_lock

      void unlock() noexcept {
        lock_.store(false, std::memory_order_release);
      } // unlock

    }; // spin_lock

} // namespace spin_ttas

#endif // SPIN_LOCK_HPP
